let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/* Stylesheets */

/* Dashboard */

mix.sass('resources/assets/sass/app.scss',
    'public/assets/dashboard/css/app.css');

mix.js([
    'resources/assets/js/dashboard/plugins/perfect-scrollbar.jquery.min.js',
    'resources/assets/js/dashboard/plugins/chartjs.min.js',
    'resources/assets/js/dashboard/plugins/bootstrap-notify.js',
    'resources/assets/js/dashboard/paper-dashboard.min.js',
], 'public/assets/dashboard/js/app.js');

mix.copy([
    'resources/assets/sass/fonts',
], 'public/assets/dashboard/fonts');

/* / Dashboard */

mix.styles([
    'resources/assets/css/bootstrap.css',
], 'public/assets/css/bootstrap.css');

mix.copy([
    'resources/assets/css/main.css',
], 'public/assets/css');

mix.copy([
    'node_modules/jquery-mask-plugin/dist/jquery.mask.min.js',
], 'public/assets/js/jquery.mask.js');

mix.copy([
    'resources/assets/js/dashboard/mask.js',
], 'public/assets/js/mask.js');


mix.copy([
    'resources/assets/css/login/animate.css',
    'resources/assets/css/login/hamburgers.min.css',
    'resources/assets/css/login/select2.min.css',
    'resources/assets/css/login/util.css',
    'resources/assets/css/login/main.css',
], 'public/assets/login/css/main.all.css');

/* Scripts */

/* login */

mix.copy([
    'resources/assets/js/login/select2.min.js',
    'resources/assets/js/login/main.js',
], 'public/assets/login/js/main.all.js');

mix.copy([
    'resources/assets/js/login/tilt.jquery.min.js',
], 'public/assets/login/js/tilt.jquery.min.js');

/* /Login */

mix.copy([
    'resources/assets/js/jquery.min.js',
], 'public/assets/js/jquery.min.js');

mix.copy([
    'resources/assets/js/bootstrap.min.js',
], 'public/assets/js/bootstrap.min.js');

mix.copy([
    'resources/assets/js/jquery.easing.min.js',
], 'public/assets/js/jquery.easing.min.js');

// mix.copy([
//     'resources/assets/js/youtube.api.js',
// ], 'public/assets/js/youtube.api.js');

mix.copy([
    'resources/assets/js/main.js',
], 'public/assets/js/main.js');

/* Images e Fontes */

mix.copy([
    'resources/assets/images',
], 'public/assets/images');

mix.copy([
    'resources/assets/css/login/fontes',
], 'public/assets/login/fontes');
