<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'first_name'=> 'Victor',
            'last_name' => 'Vieira',
            'email' => 'viih@live.fr',
            'password' => bcrypt('123456'),
            'telephone' => '14996726316',
            'type_musician_id' => '2'
        ];

        User::create($user);

    }
}
