<?php

use Illuminate\Database\Seeder;
use App\Models\TypesMusician;

class TypesMusicianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vocalist = [
            'musician' => 'Vocalista',
        ];

        TypesMusician::create($vocalist);


        $bassist = [
            'musician' => 'Baixista',
        ];

        TypesMusician::create($bassist);

        $guitarrist = [
            'musician' => 'Guitarrista',
        ];

        TypesMusician::create($guitarrist);

        $drummer = [
            'musician' => 'Baterista',
        ];

        TypesMusician::create($drummer);
    }
}
