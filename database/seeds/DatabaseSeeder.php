<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(tableStatesSeeder::class);
        $this->call(tableCitiesSeeder::class);
        $this->call(TypesMusicianSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(BandsSeeder::class);
    }
}
