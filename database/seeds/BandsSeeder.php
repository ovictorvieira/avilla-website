<?php

use Illuminate\Database\Seeder;
use App\Models\Bands;

class BandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $band = [
            'name'=> '',
            'description'=> '',
            'repertoire_summary'=> '',
        ];

        Bands::create($band);
    }
}
