<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bands', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name', 120);
            $table->string('description', 400);
            $table->string('repertoire_summary', 200);
            $table->string('responsible_name', 50)->nullable();
            $table->string('responsible_phone', 50)->nullable();
            $table->string('responsible_email', 50)->nullable();

            $table->string('social_media_facebook', 180)->nullable();
            $table->string('social_media_twitter', 180)->nullable();
            $table->string('social_media_instagram', 180)->nullable();
            $table->string('social_media_youtube', 180)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bands');
    }
}
