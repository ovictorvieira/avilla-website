<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('telephone');
            $table->string('email')->unique();
            $table->string('password');

            $table->string('social_media_facebook_link', 150)->nullable();
            $table->string('social_media_instagram_link', 150)->nullable();
            $table->string('social_media_twitter_link', 150)->nullable();

            $table->string('principal_image', 150)->nullable();

            $table->integer('type_musician_id')->unsigned();

            $table->foreign('type_musician_id')->references('id')->on('types_musician');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
