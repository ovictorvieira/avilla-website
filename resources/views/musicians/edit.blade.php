@extends('layouts.dash')

@section('content')

    <!-- main-painel -->
    <div class="main-panel">

        @include('menu.menu')

        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="image">
                            <img src="{{ asset('assets/images/banner/damir-bosnjak.jpg') }}" alt="Imagem de banner padrão">
                        </div>
                        <div class="card-body">
                            <div class="author">
                                <img class="avatar border-gray" src="{{ Auth::user()->present()->returnMemberImage }}" alt="Imagem de perfil padrão">
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Integrantes Cadastrados</h4>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled team-members">

                                @if($members->count() > 0)
                                    @foreach($members as $member)
                                        <li>
                                            <div class="row">
                                                <div class="col-md-2 col-2">
                                                    <div class="avatar">
                                                        <img src="{{ $member->present()->returnMemberImage }}" alt="Imagem de perfil do usuário {{ $member->present()->returnUserName }}" class="img-circle img-no-padding img-responsive">
                                                    </div>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    {{ $member->present()->returnUserName }}
                                                    <br />
                                                    <span class="text-muted">
                                                <small>{{ $member->typeMusician->musician }}</small>
                                            </span>
                                                </div>
                                                <div class="col-md-3 col-3 text-right">
                                                    <a href="{{ route($member->present()->memberIsUserLogged($member->id), $member->id) }}">
                                                        <button class="btn btn-sm btn-outline-success btn-round btn-icon"><i class="fas fa-eye"></i></button>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    <li>
                                        <div class="row">
                                            <div class="col-md-12 col-12">
                                                Não há integrantes cadastrados
                                            </div>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card card-user">
                        <div class="card-header">
                            <h5 class="card-title">Dados do Integrante {{ Auth::user()->first_name }}</h5>
                            <span class="text-muted">
                                <small>Obs.: Caso queira alterar algum campo, os campos com <span class="font-weight-bold">*</span> são obrigatórios</small>
                            </span>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ route('memberUpdate', Auth::user()->id) }}" enctype="multipart/form-data">

                                {{ csrf_field() }}

                                <input type="hidden" name="_method" value="PUT">

                                <div class="row">
                                    <div class="col-md-5 pr-1">
                                        <div class="form-group">
                                            <label>Banda</label>
                                            <input type="text" class="form-control" disabled placeholder="Nome da Banda" value="{{ $band->name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 px-1">
                                        <div class="form-group">
                                            <label for="first_name">Nome *</label>
                                            <input type="text" class="form-control" value="{{ Auth::user()->first_name }}" name="first_name" id="first_name">
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-1">
                                        <div class="form-group">
                                            <label for="last_name">Sobrenome *</label>
                                            <input type="text" class="form-control" value="{{ Auth::user()->last_name }}" name="last_name" id="last_name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label>E-mail *</label>
                                            <input type="email" class="form-control" value="{{ Auth::user()->email }}" name="email">
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-1">
                                        <div class="form-group">
                                            <label>Senha</label>
                                            <input type="password" class="form-control" placeholder="Caso queira trocar a sua senha, altere-a aqui..." name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="type_musician_id">Selecione um tipo músico *</label>
                                        <select class="form-control" id="type_musician_id">
                                            @foreach($typesMusician as $typeMusician)
                                                @if($typeMusician->id == $member->type_musician_id)
                                                    <option value="{{ $typeMusician->id }}" selected>{{ $typeMusician->musician }}</option>
                                                @endif
                                                <option value="{{ $typeMusician->id }}">{{ $typeMusician->musician }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6 pl-1">
                                        <div class="form-group">
                                            <label>Telefone</label>
                                            <input type="text" class="form-control phone_with_ddd" value="{{ Auth::user()->telephone }}" name="telephone">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group">
                                            <label>Facebook</label>
                                            <input type="text" class="form-control" value="{{ Auth::user()->present()->returnSocialMediaLink('facebook') }}" name="social_media_facebook_link">
                                        </div>
                                    </div>
                                    <div class="col-md-4 px-1">
                                        <div class="form-group">
                                            <label>Instagram</label>
                                            <input type="text" class="form-control" value="{{ Auth::user()->present()->returnSocialMediaLink('instagram') }}" name="social_media_instagram_link">
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-1">
                                        <div class="form-group">
                                            <label>Twitter</label>
                                            <input type="text" class="form-control" value="{{ Auth::user()->present()->returnSocialMediaLink('twitter') }}" name="social_media_twitter_link">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="h6">O Arquivo Precisa ser de até 5mb</span>
                                        <br>
                                        <span class="h6">Caso a imagem seja grande, você pode reduzi-la <a href="https://compressjpeg.com/pt/" target="_blank">neste site</a>.</span>
                                        <br>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="principal_image" id="principal_image">
                                            <label class="custom-file-label pt-2" for="principal_image">Selecione uma imagem...</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="update ml-auto mr-auto">
                                        <button type="submit" class="btn btn-primary">Atualizar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('menu.footer')

    </div>
    <!-- End main-painel -->

@endsection
