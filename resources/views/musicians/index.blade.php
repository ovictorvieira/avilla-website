@extends('layouts.dash')

@section('content')

    <!-- main-painel -->
    <div class="main-panel">

        @include('menu.menu')

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-user">
                        <div class="card-header">
                            <h5 class="card-title">Integrantes Cadastrados</h5>
                        </div>
                        <div class="card-body">

                            @if($users->count() > 0)

                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                            <tr>
                                                <th class="text-center">Foto</th>
                                                <th class="text-left">Nome</th>
                                                <th class="text-left">E-mail</th>
                                                <th class="text-left">Tipo de Músico</th>
                                                <th class="text-center">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td class="text-center">
                                                    <img class="img-circle img-table" src="{{ $user->present()->returnMemberImage }}" alt="Imagem de perfil do usuario {{ $user->name }}">
                                                </td>
                                                <td class="text-left">{{ $user->present()->returnUserName }}</td>
                                                <td class="text-left">{{ $user->email }}</td>
                                                <td class="text-left">{{ $user->typeMusician->musician }}</td>
                                                <td class="text-center">
                                                    <a href="{{ route($user->present()->memberIsUserLogged($user->id), $user->id) }}">
                                                        <button type="submit" class="btn btn-default">Visualizar</button>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="row pt-4">
                                    <div class="col-md-12">
                                        <h5 class="subheading text-center">Não há integrantes cadastrados. Faça o cadastro abaixo.</h5>
                                    </div>
                                </div>
                            @endif
                            <div class="d-flex justify-content-end pt-4">

                                <a href="{{ route('memberNew') }}">
                                    <button type="submit" class="btn btn-success btn-round">Cadastrar</button>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('menu.footer')

    </div>
    <!-- End main-painel -->

@endsection
