
<div class="sidebar" data-color="black" data-active-color="blue">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('assets/images/logos/logo-preta-fundo-branco.jpg') }}">
            </div>
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            A VILLA
            <!-- <div class="logo-image-big">
              <img src="../assets/img/logo-big.png">
            </div> -->
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ request()->is('home') ? 'active' : '' }}">
                <a href="{{ route('home') }}">
                    <i class="fas fa-home"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="{{ request()->is('band') ? 'active' : '' }}">
                <a href="{{ route('band') }}">
                    <i class="material-icons">phonelink</i>
                    <p>Dados da Banda</p>
                </a>
            </li>
            <li class="{{ request()->is('members') ? 'active' : '' }}">
                <a href="{{ route('members') }}">
                    <i class="material-icons">people</i>
                    <p>Integrantes</p>
                </a>
            </li>
            <li class="#">
                <a href="#">
                    <i class="material-icons">event_available</i>
                    <p>Agenda</p>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="material-icons">speaker_group</i>
                    <p>Eventos</p>
                </a>
            </li>
            <li>
                <a href="">
                    <i class="material-icons">ondemand_video</i>
                    <p>Videos</p>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="far fa-money-bill-alt"></i>
                    <p>Caixa</p>
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off"></i>
                    <p>Sair</p>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </a>
            </li>

        </ul>
    </div>
</div>
