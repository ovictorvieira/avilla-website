<!-- Footer -->
<footer class="footer footer-black  footer-white ">
    <div class="container-fluid">
        <div class="row">
            <div class="credits ml-auto">
                Copyright ©<a href="{{ asset('/') }}" target="_blank"> A Villa </a>{{ date('Y') }}.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->
