@extends('layouts.dash')

@section('content')

    <!-- main-painel -->
    <div class="main-panel">

        @include('menu.menu')

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-user">
                        <div class="card-header">
                            <h5 class="card-title">Descrição do Site</h5>
                        </div>
                        <div class="card-body">

                            <form method="post" action="{{ route('bandUpdate', $band->id ) }}">

                                {{ csrf_field() }}

                                <input type="hidden" name="_method" value="PUT">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Nome da Banda</label>
                                            <input type="text" name="name" class="form-control" placeholder="Nome da Banda" value="{{ $band->name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Descrição</label>
                                            <textarea name="description" class="form-control textarea">{{ $band->description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Resumo do Repertório</label>
                                            <textarea name="repertoire_summary" class="form-control textarea">{{ $band->repertoire_summary or 'Primeira banda, Segunda banda, Terceira banda...'}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 pr-1">
                                        <div class="form-group">
                                            <label>Link do Facebook</label>
                                            <input type="text" name="social_media_facebook" class="form-control" placeholder="Link do perfil da sua banda no Facebook" value="{{ $band->social_media_facebook }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 px-1">
                                        <div class="form-group">
                                            <label>Link do Instagram</label>
                                            <input type="text" name="social_media_instagram" class="form-control" placeholder="Link do perfil da sua banda no Instagram" value="{{ $band->social_media_instagram }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 pl-1">
                                        <div class="form-group">
                                            <label>Link do Twitter</label>
                                            <input type="text" name="social_media_twitter" class="form-control" placeholder="Link do perfil da sua banda no Twitter" value="{{ $band->social_media_twitter }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 pl-1">
                                        <div class="form-group">
                                            <label>Link do Youtube</label>
                                            <input type="text" name="social_media_youtube" class="form-control" placeholder="Link do perfil da sua banda no Youtube" value="{{ $band->social_media_youtube }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="card-header">
                                    <h5 class="card-title">Dados do Responsável pelas Vendas</h5>
                                    <span class="text-muted">Obs.: Estes dados aparecem no site, na parte "Contrate a Banda...".</span>
                                </div>
                                <div class="row pt-4">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nome do Responsável</label>
                                            <input type="text" name="responsible_name" class="form-control" value="{{ $band->responsible_name }}" placeholder="Nome do responsável">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Telefone de Contato do Responsável</label>
                                            <input type="text" name="responsible_phone" class="form-control phone_with_ddd_sp" value="{{ $band->responsible_phone }}" placeholder="Telefone do responsável">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>E-mail do Responsável</label>
                                            <input type="email" name="responsible_email" class="form-control" value="{{ $band->responsible_email }}" placeholder="E-mail do responsável">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="update ml-auto mr-auto">
                                        <button type="submit" class="btn btn-primary">Atualizar Descrição</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('menu.footer')

    </div>
    <!-- End main-painel -->

@endsection
