@extends('layouts.dash')

@section('content')

    <!-- main-painel -->
    <div class="main-panel">

        @include('menu.menu')

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-user">
                        <div class="card-header">
                            <h5 class="card-title">Descrição do Site</h5>
                        </div>
                        <div class="card-body">

                            @if($band->count() > 0)
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <h2 class="section-heading text-uppercase">Sobre a Banda</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p class="text-muted text-center">
                                            {{ $band->description }}
                                        </p>

                                        <br>

                                        <div class="timeline-heading">
                                            <h5 class="subheading text-center">O Repertório é formado com músicas de bandas como:</h5>
                                        </div>

                                        <br>

                                        <p class="text-muted text-center">
                                            <strong>{{ $band->repertoire_summary }}</strong>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mx-auto">
                                        <a href="{{ route('bandEdit', $band->id) }}">
                                            <button type="submit" class="btn btn-primary">Editar Dados</button>
                                        </a>

                                        <span class="px-3"></span>

                                        <a href="{{ route('landingPageShow') }}#about" target="_blank">
                                            <button type="submit" class="btn btn-success">Visualizar Site</button>
                                        </a>
                                    </div>
                                </div>
                            @else
                                <div class="row pt-4">
                                    <div class="col-md-12">
                                        <h5 class="subheading text-center">Não há dados cadastrado. Faça o cadastro das informações abaixo.</h5>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center pt-4">

                                    <div class="mx-auto">
                                        <a href="{{ route('bandCreate') }}">
                                            <button type="submit" class="btn btn-success">Cadastrar Informações</button>
                                        </a>

                                        <span class="px-3"></span>

                                        <a href="{{ route('landingPageShow') }}#about" target="_blank">
                                            <button type="submit" class="btn btn-success">Visualizar Site</button>
                                        </a>
                                    </div>

                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('menu.footer')

    </div>
    <!-- End main-painel -->

@endsection
