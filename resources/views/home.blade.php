@extends('layouts.dash')

@section('content')

    <!-- main-painel -->
    <div class="main-panel">

        @include('menu.menu')

        <div class="content">

            @if (session()->has('msg'))
                <div class="alert alert-{{ session('type_msg') }} alert-dismissible fade show">
                    <span> {{ session('msg') }} </span>
                </div>
            @endif

            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                        <img src="{{ asset('assets/images/logos/facebook.svg') }}">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">Curtidas</p>
                                        <p class="card-title">156
                                        <p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">
                                <i class="fa fa-refresh"></i> Diretamente do Facebook
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                        <img src="{{ asset('assets/images/logos/instagram.svg') }}">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">Seguidores</p>
                                        <p class="card-title">156
                                        <p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">
                                <i class="fa fa-calendar-o"></i> Diretamente do Instagram
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                        <img src="{{ asset('assets/images/logos/youtube.svg') }}">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">Inscritos</p>
                                        <p class="card-title">23
                                        <p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">
                                <i class="fa fa-clock-o"></i> Diretamente do Youtube
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                        <img src="{{ asset('assets/images/logos/twitter.svg') }}">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">Seguidores</p>
                                        <p class="card-title">148
                                        <p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">
                                <i class="fa fa-refresh"></i> Diretamente do Twitter
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card ">
                        <div class="card-header">
                            <h5 class="card-title">Movimentação do Dinheiro da Banda</h5>
                        </div>
                        <div class="card-body ">
                            <canvas id=chartHours width="400" height="72"></canvas>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">
                                <i class="fa fa-history"></i> Atualizado
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card ">
                        <div class="card-header">
                            <h5 class="card-title">Caixa da Banda</h5>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4 d-flex justify-content-center">
                                    <div class="icon-big text-center icon-warning">
                                        <img src="{{ asset('assets/images/logos/money.svg') }}">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers text-right d-flex justify-content-center">
                                        <p class="card-category mt-3">R$ 154,00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">
                                <i class="fa fa-history"></i> Atualizado
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @include('menu.footer')

    </div>
    <!-- End main-painel -->

@endsection
