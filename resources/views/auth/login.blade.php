@extends('layouts.app')

@section('content')

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="{{ asset('assets/images/banner/login-image.png') }}" alt="IMG">
            </div>

            <form class="login100-form validate-form" method="post" action="{{ route('login') }}">
                <span class="login100-form-title">
                    Login
                </span>

                {{ csrf_field() }}

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

                <div class="wrap-input100 validate-input{{ $errors->has('email') ? ' has-error' : '' }}" data-validate = "Insira um e-mail válido: exemplo@exemplo.com">
                    <input class="input100" type="text" name="email" placeholder="E-mail" value="{{ old('email') }}" required>

                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>
                </div>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

                <div class="wrap-input100 validate-input{{ $errors->has('password') ? ' has-error' : '' }}" data-validate = "É necessário informar sua senha">
                    <input class="input100" type="password" name="password" placeholder="Senha">

                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        <i class="fas fa-sign-in-alt"></i>&nbsp;Login
                    </button>
                </div>

                <div class="text-center pt-3">
                    <span class="txt1">
                        Esqueceu sua&nbsp;
                    </span>
                    <a class="txt2" href="{{ route('password.request') }}">
                        Senha?
                    </a>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
