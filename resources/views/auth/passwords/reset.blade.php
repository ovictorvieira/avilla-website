@extends('layouts.app')

@section('content')

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-pic js-tilt d-flex align-items-center" data-tilt>
                    <img src="{{ asset('assets/images/banner/login-image.png') }}" alt="IMG">
                </div>

                <form class="login100-form validate-form" method="post" action="{{ route('password.request') }}">

                    <span class="login100-form-title">
                        Resetar Senha
                    </span>

                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <div class="wrap-input100 validate-input{{ $errors->has('email') ? ' has-error' : '' }}" data-validate = "É necessário informar sua senha">
                        <input id="email" type="email" class="input100" name="email" placeholder="Informe o seu e-mail" value="{{ old('email') }}" required>

                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    <div class="wrap-input100 validate-input{{ $errors->has('password') ? ' has-error' : '' }}" data-validate = "É necessário informar seu e-mail">
                        <input id="password" type="password" class="input100" name="password" placeholder="Senha" required>

                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif

                    <div class="wrap-input100 validate-input{{ $errors->has('password_confirmation') ? ' has-error' : '' }}" data-validate = "As senhas informadas precisam ser iguais">
                        <input id="password-confirm" type="password" class="input100" name="password_confirmation" placeholder="Confirme sua senha" value="{{ old('email') }}" required>

                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            <i class="fab fa-envira"></i>&nbsp;Resetar Senha
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection
