@extends('layouts.app')

@section('content')

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-pic js-tilt d-flex align-items-center" data-tilt>
                    <img src="{{ asset('assets/images/banner/login-image.png') }}" alt="IMG">
                </div>

                <form class="login100-form validate-form" method="post" action="{{ route('password.email') }}">

                    <span class="login100-form-title">
                        Resetar Senha
                    </span>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ csrf_field() }}

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <div class="wrap-input100 validate-input{{ $errors->has('email') ? ' has-error' : '' }}" data-validate = "É necessário informar sua senha">
                        <input id="email" type="email" class="input100" name="email" placeholder="Informe o seu e-mail" value="{{ old('email') }}" required>

                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            <i class="fab fa-envira"></i>&nbsp;Enviar
                        </button>
                    </div>

                    <div class="text-center pt-3">
						<span class="txt1">
							Já possui uma conta?&nbsp;
						</span>
                        <a class="txt2" href="{{ route('login') }}">
                            Efetuar login
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
