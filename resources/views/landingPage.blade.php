<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Página da Banda A Villa de Marília, SP.">
        <meta name="author" content="Victor Hugo de Souza Vieira">

        <title>{{ config('app.name') }}</title>

        <link rel="shortcut icon" type="image/jpg" href="{{ asset('assets/images/logos/logo-branca.jpg') }}" />

        <!-- Custom fonts for this template -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

        <!-- Custom styles for this template -->
        <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">

    </head>

    <body id="page-top">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container-fluid">
                <button class="navbar-toggler navbar-toggler-right" id="menuButton" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#page-top">Início</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#ultimosEventos">Eventos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#about">Sobre</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#Integrantes">Integrantes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#agenda">Agenda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#featureVideos">Videos em Destaque</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#formContact">Contato</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Header -->
        <header class="masthead">
            <div class="container">
                <div class="intro-text">

                </div>
            </div>
        </header>

        <!-- Ultimos Eventos da Banda -->
        <section id="ultimosEventos">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading text-uppercase">Ultimos Eventos</h2>
                        <h3 class="section-subheading text-muted">Veja alguns videos e fotos dos ultimos eventos da {{ $band->name }}.</h3>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 px-2">
                        <div class="card card-shadow">
                            <div class="card-header text-right">
                                19 Ago 2017
                            </div>
                            <div class="card-img">
                                <img class="img-fluid" src="{{ asset('assets/images/events/caoPerere-2017/img-2.jpg') }}" alt="Banda A Villa abrindo o show do Tributo Cazuza e Barão Vermelho.">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Cão Pererê - Marília, SP</h5>
                                <p class="card-text">Banda A Villa abrindo o show da banda Exagerado Tributo Cazuza e Barão Vermelho.</p>
                                <a data-toggle="modal" href="#portfolioModal1" class="btn btn-custom">Mais Detalhes</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 px-2">
                        <div class="card card-shadow">
                            <div class="card-header text-right">
                                14 Abr 2018
                            </div>
                            <div class="card-img">
                                <img class="img-fluid" src="{{ asset('assets/images/events/caoPerere-2018/img-2.jpg') }}" alt="A Banda A Villa retorna ao palco do Cão Pererê.">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Cão Pererê - Marília, SP</h5>
                                <p class="card-text">A Banda A Villa retorna ao palco do Cão Pererê.</p>
                                <a data-toggle="modal" href="#portfolioModal2" class="btn btn-custom">Mais Detalhes</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 px-2">
                        <div class="card card-shadow">
                            <div class="card-header text-right">
                                23 Jul 2018
                            </div>
                            <div class="card-img">
                                <img class="img-fluid" src="{{ asset('assets/images/events/encontro-2018/img-0.jpg') }}" alt="A Banda A Villa apresenta um show especial no Encontro do Rock.">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Espaço Cultural - Marília, SP</h5>
                                <p class="card-text">A Banda A Villa apresenta um show especial no Encontro do Rock.</p>
                                <a data-toggle="modal" href="#portfolioModal3" class="btn btn-custom">Mais Detalhes</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 px-2">
                        <div class="card card-shadow">
                            <div class="card-header text-right">
                                01 Set 2018
                            </div>
                            <div class="card-img">
                                <img class="img-fluid" src="{{ asset('assets/images/events/capitao-2018/img-0.jpg') }}" alt="Banda A Villa abrindo o show do Tributo Cazuza e Barão Vermelho.">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Capitão Beer - Marília, SP</h5>
                                <p class="card-text">A Banda A Villa sobe ao palco do Capitão Beer e apresenta um repertório cheio de energia.</p>
                                <a data-toggle="modal" href="#portfolioModal4" class="btn btn-custom">Mais Detalhes</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <!-- Sobre a Banda -->
        <section id="about" class="pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading text-uppercase">Sobre a Banda</h2>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <p class="text-muted text-center">
                                    {{ $band->description }}
                                </p>

                                <br>

                                <div class="timeline-heading">
                                    <h5 class="subheading text-center">O Repertório é formado com músicas de bandas como:</h5>
                                </div>

                                <br>

                                <p class="text-muted text-center">
                                    <strong>
                                        {{ $band->repertoire_summary }}
                                    </strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Integrantes -->
        <section class="bg-light" id="Integrantes">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading text-uppercase">Integrantes</h2>
                        <h3 class="section-subheading text-muted">Músicos que formam a banda {{ $band->name }}.</h3>
                    </div>
                </div>
                <div class="row">
                    @foreach($members as $member)
                        <div class="col-sm-3">
                            <div class="team-member">
                                <img class="mx-auto rounded-circle" src="{{ asset( $member->present()->returnMemberImage) }}" alt="{{ $member->present()->returnUserName }} - {{ $member->typeMusician->musician }} da banda {{ $band->name }}">
                                <h4>{{ $member->present()->returnUserName }}</h4>
                                <p class="text-muted">{{ $member->typeMusician->musician }}</p>
                                <ul class="list-inline social-buttons">
                                    <li class="list-inline-item">
                                        <a href="{{ $member->social_media_facebook_link }}" target="_blank">
                                            <i class="fab fa-facebook-square"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{ $member->social_media_instagram_link }}" target="_blank">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        <!-- Agenda -->
        <section id="agenda" class="section-container">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10 offset-lg-1">
                        <div class="section-header">
                            <h1 class="section-title">Eventos</h1>
                        </div>
                        <div style="height: 30px;"></div>
                    </div>
                </div>

                <div class="row">
                    <div style="display: none;" class="offset-lg-1 col-lg-10 offset-lg-1">
                        <div class="section-body listing">
                            <article class="event">
                                <div class="event-date">

                                </div>
                                <div class="event-title">
                                    <h2>Dia Mes Ano<span>Local</span></h2>
                                    <h6>Endereço Cidade, Estado</h6>
                                </div>
                                <div class="event-links">
                                    <a class="btn btn-default" href="#"><span>Mais Detalhes</span></a>
                                </div>

                            </article>
                        </div>
                    </div>

                    <div class="offset-lg-1 col-lg-10 offset-lg-1">
                        <div class="section-body listing">
                            <article class="event">
                                <div class="event-date">

                                </div>
                                <div class="event-title">
                                    <h2>Em Breve</h2>
                                </div>

                            </article>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <!-- Parceiros -->

        <section class="py-5" style="display: none;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading text-uppercase">Parcerias</h2>
                        <h3 class="section-subheading text-muted">Alguns parceiros da banda.</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <a href="#">
                            <img class="img-fluid d-block mx-auto" src="img/logos/envato.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <a href="#">
                            <img class="img-fluid d-block mx-auto" src="img/logos/designmodo.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <a href="#">
                            <img class="img-fluid d-block mx-auto" src="img/logos/themeforest.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <a href="#">
                            <img class="img-fluid d-block mx-auto" src="img/logos/creative-market.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Videos do Youtube -->

        <section id="featureVideos" class="bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <h2 class="section-heading text-uppercase">Videos em Destaque</h2>
                        <h3 class="section-subheading text-muted">Assista alguns videos de apresentações ao vivo da banda <strong>{{ $band->name }}</strong>.</h3>
                    </div>
                </div>

                <div class="row">

                @foreach($videoList as $video)

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-4 px-2 zoom">
                        <iframe class="w-video" src="https://www.youtube.com/embed/{{ $video->id->videoId }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>

                @endforeach

                </div>
            </div>
        </section>

        <!-- Formulario de Contato -->
        <div class="contact-fade">
            <section id="formContact">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <img class="img-fluid" src="{{ asset('assets/images/logos/logo-branca.jpg') }}">
                        </div>

                        <div class="offset-lg-1 offset-md-1 col-lg-7 col-md-7 text-left">
                            <div class="box-contact-us-right">
                                <h2 class="section-heading font-weight-normal">Contrate {{ $band->name }}</h2>
                                <p>
                                <h3 class="text-uppercase">Contatos</h3>
                                <br>
                                <div class="d-flex align-items-center box-contact-us mb-3">
                                    <i class="fab fa-whatsapp fa-3x"></i>
                                    <h5 class="text-muted mb-0 pl-3"><a href="https://api.whatsapp.com/send?phone={{ $band->responsible_phone }}&text=Olá%{{ str_replace(' ', '%20', $band->responsible_name) }},%20Tudo%20bem?%20Encontrei%20seu%20contato%20pelo%20site%20da%20{{ str_replace(' ', '%20', $band->name) }}.%20Gostaria%20de%20falar%20sobre%20algumas%20datas,%20podemos%20conversar?%20">{{ $band->responsible_phone }}</a> - <span class="text-body">{{ $band->responsible_name }}</span></h5>
                                </div>
                                <div class="d-flex align-items-center box-contact-us mb-3">
                                    <i class="fas fa-envelope fa-3x"></i>
                                    <button type="submit" class="btn btn-info ml-3">Enviar E-mail</button>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <span class="copyright">Copyright &copy; {{ $band->name }} {{ date('Y') }}</span>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-inline social-buttons">
                            <li class="list-inline-item list-inline-item-bottom">
                                <a href="{{ $band->social_media_facebook }}" target="_blank">
                                    <i class="fab fa-facebook-square fab-icon-custom-footer"></i>
                                </a>
                            </li>
                            <li class="list-inline-item list-inline-item-bottom">
                                <a href="{{ $band->social_media_instagram }}" target="_blank">
                                    <i class="fab fa-instagram fab-icon-custom-footer"></i>
                                </a>
                            </li>
                            <li class="list-inline-item list-inline-item-bottom">
                                <a href="{{ $band->social_media_youtube }}" target="_blank">
                                    <i class="fab fa-youtube fab-icon-custom-footer"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
        </footer>

        <!--Modals Ultimos eventos -->

        <!-- Modal 1 -->
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl"></div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row container-mobile">
                            <div class="offset-lg-1 col-lg-6">
                                <div id="carouselControlsModal1" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox">
                                        <div class="carousel-item active">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/caoPerere-2017/img-1.jpg') }}" alt="A banda A Villa retorna ao palco do Cão Pererê para mais uma noite inesquecível.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/caoPerere-2017/img-2.jpg') }}" alt="A banda A Villa retorna ao palco do Cão Pererê para mais uma noite inesquecível.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/caoPerere-2017/img-3.jpg') }}" alt="A banda A Villa retorna ao palco do Cão Pererê para mais uma noite inesquecível.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/caoPerere-2017/img-4.jpg') }}" alt="A banda A Villa retorna ao palco do Cão Pererê para mais uma noite inesquecível.">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselControlsModal1" role="button" data-slide="prev">
                                        <i class="fas fa-angle-left angle-left-custom fa-3x"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselControlsModal1" role="button" data-slide="next">
                                        <i class="fas fa-angle-right angle-left-custom fa-3x"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-4 text-left">
                                <div class="box-contact-us-right">
                                    <h2 class="section-heading text-body">Cão Pererê <br> Márilia, SP</h2>
                                    <p>
                                    <h5 class="text-muted font-weight-normal">
                                        Banda A Villa no palco do Cão Pererê fazendo a abertura com chave de ouro para a banda Exagerado Tributo Cazuza e Barao Vermelho.
                                    </h5>
                                    </p>
                                </div>
                            </div>
                            <div class="offset-lg-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal 2 -->
        <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl"></div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row container-mobile">
                            <div class="offset-lg-1 col-lg-6">
                                <div id="carouselControlsModal2" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox">
                                        <div class="carousel-item active">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/caoPerere-2018/img-1.jpg') }}" alt="Banda A Villa no palco do Cão Pererê fazendo a abertura com chave de ouro para a banda Exagerado Tributo Cazuza e Barao Vermelho.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/caoPerere-2018/img-2.jpg') }}" alt="Banda A Villa no palco do Cão Pererê fazendo a abertura com chave de ouro para a banda Exagerado Tributo Cazuza e Barao Vermelho.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/caoPerere-2018/img-3.jpg') }}" alt="Banda A Villa no palco do Cão Pererê fazendo a abertura com chave de ouro para a banda Exagerado Tributo Cazuza e Barao Vermelho.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/caoPerere-2018/img-4.jpg') }}" alt="Banda A Villa no palco do Cão Pererê fazendo a abertura com chave de ouro para a banda Exagerado Tributo Cazuza e Barao Vermelho.">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselControlsModal2" role="button" data-slide="prev">
                                        <i class="fas fa-angle-left angle-left-custom fa-3x"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselControlsModal2" role="button" data-slide="next">
                                        <i class="fas fa-angle-right angle-left-custom fa-3x"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-4 text-left">
                                <div class="box-contact-us-right">
                                    <h2 class="section-heading text-body">Cão Pererê <br> Márilia, SP</h2>
                                    <p>
                                    <h5 class="text-muted font-weight-normal">
                                        A banda A Villa retorna ao palco do Cão Pererê para mais uma noite inesquecível.
                                    </h5>
                                    </p>
                                </div>
                            </div>

                            <div class="offset-lg-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal 3 -->
        <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl"></div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row container-mobile">
                            <div class="offset-lg-1 col-lg-6">
                                <div id="carouselControlsModal3" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox">
                                        <div class="carousel-item active">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/encontro-2018/img-1.jpg') }}" alt="A Banda A Villa apresenta um show especial no Encontro do Rock.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/encontro-2018/img-2.jpg') }}" alt="A Banda A Villa apresenta um show especial no Encontro do Rock.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/encontro-2018/img-3.jpg') }}" alt="A Banda A Villa apresenta um show especial no Encontro do Rock.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/encontro-2018/img-4.jpg') }}" alt="A Banda A Villa apresenta um show especial no Encontro do Rock.">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselControlsModal3" role="button" data-slide="prev">
                                        <i class="fas fa-angle-left angle-left-custom fa-3x"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselControlsModal3" role="button" data-slide="next">
                                        <i class="fas fa-angle-right angle-left-custom fa-3x"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-4 text-left">
                                <div class="box-contact-us-right">
                                    <h2 class="section-heading text-body">Encontro do Rock <br> Márilia, SP</h2>
                                    <p>
                                    <h5 class="text-muted font-weight-normal">
                                        A Banda A Villa apresentou um show especial no famoso Encontro do Rock, evento presente na cidade de Marília a anos.
                                    </h5>
                                    </p>
                                </div>
                            </div>
                            <div class="offset-lg-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal 4 -->
        <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl"></div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row container-mobile">
                            <div class="offset-lg-1 col-lg-6">
                                <div id="carouselControlsModal4" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox">
                                        <div class="carousel-item active">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/capitao-2018/img-1.jpg') }}" alt="A Banda A Villa subiu ao palco do Capitão Beer e fez um mega show onde o público cantou todas as músicas do repertório.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/capitao-2018/img-2.jpg') }}" alt="A Banda A Villa subiu ao palco do Capitão Beer e fez um mega show onde o público cantou todas as músicas do repertório.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/capitao-2018/img-3.jpg') }}" alt="A Banda A Villa subiu ao palco do Capitão Beer e fez um mega show onde o público cantou todas as músicas do repertório.">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" src="{{ asset('assets/images/events/capitao-2018/img-4.jpg') }}" alt="A Banda A Villa subiu ao palco do Capitão Beer e fez um mega show onde o público cantou todas as músicas do repertório.">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselControlsModal4" role="button" data-slide="prev">
                                        <i class="fas fa-angle-left angle-left-custom fa-3x"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselControlsModal4" role="button" data-slide="next">
                                        <i class="fas fa-angle-right angle-left-custom fa-3x"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-4 text-left">
                                <div class="box-contact-us-right">
                                    <h2 class="section-heading text-body">Capitão Beer <br> Márilia, SP</h2>
                                    <p>
                                    <h5 class="text-muted font-weight-normal">
                                        A Banda A Villa subiu ao palco do Capitão Beer e fez um mega show onde o público cantou todas as músicas do repertório.
                                    </h5>
                                    </p>
                                </div>
                            </div>
                            <div class="offset-lg-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>

        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('assets/js/youtube.api.js') }}"></script>
        <script src="{{ asset('assets/js/main.js') }}"></script>

        <script type="text/javascript">
            document.getElementById("menuButton").addEventListener("click", function() {
                if (!$('#navbarResponsive').hasClass('show')) {
                    $('#mainNav').addClass('fadeMenuOpen');
                } else {
                    $('#mainNav').removeClass('fadeMenuOpen');
                }
            });
        </script>

    </body>

</html>
