<?php

namespace App\Presenter;

use Laracasts\Presenter\Presenter;
use Illuminate\Support\Facades\Auth;

class UsersPresenter extends Presenter
{
    /**
     * Retorna o nome do usuario concatenado
     *
     * @return string
     */
    public function returnUserName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
    
    /**
     * Retorna a imagem cadastrada de um usuário ou a imagem padrão
     *
     * @return mixed|string
     */
    public function returnMemberImage()
    {
        return (is_null($this->principal_image) ? asset('assets/images/team/default-avatar.png') : asset('/storage/users/' . $this->id . '/' . $this->principal_image));
    }

    /**
     * Retorna se o usuário tem um link de midia social cadastrado
     *
     * @param $socialMedia
     * @return string
     */
    public function returnSocialMediaLink($socialMedia)
    {
        return (is_null($this->social_media_.$socialMedia) ? 'Cadastrado' : 'Não Cadastrado' );
    }

    /**
     * Retorna se o usuário que está logado é o mesmo usuário da listagem de Membros
     *
     * @param $memberId
     * @return bool
     */
    public function memberIsUserLogged($memberId)
    {
        if(Auth::user()->id == $memberId) {
            return 'memberEdit';
        }

        return 'memberShow';
    }
}