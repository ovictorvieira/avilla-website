<?php

namespace App\Http\Controllers;

use App\Services\BandsService;
use Illuminate\Http\Request;

class BandController extends Controller
{
    /**
     * @var $bandService
     */
    protected $bandService;

    /**
     * BandController constructor.
     * @param BandsService $bandService
     */
    public function __construct(BandsService $bandService)
    {
        $this->bandService = $bandService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $band = $this->bandService->getFirstBand();

        return view('band.index', ['band' => $band]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function new()
    {
        return view('band.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        if($this->bandService->createNewBandData($request->all())) {

            return redirect('home')->with(['msg' => 'Dados cadastrados com sucesso!',
                                              'type_msg' => 'success']);
        }

        return redirect('home')->with(['msg', 'Erro ao cadastrar dados.',
                                          'type_msg' => 'danger']);
    }

    public function show()
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $band = $this->bandService->getBandById($id);

        return view('band.edit', ['band' => $band]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        if($this->bandService->updateBand($id, $request->all())) {

            return redirect('/home')->with(['msg' => 'Dados alterados com sucesso!',
                                                'type_msg' => 'success']);
        }

        return redirect('/home')->with(['msg', 'Erro ao atualizar dados.',
                                            'type_msg' => 'danger']);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy($id)
    {
        if($this->bandService->deleteBand($id)) {

            return redirect('home')->with(['msg' => 'Banda deletada com sucesso!',
                                               'type_msg' => 'success']);
        }

        return redirect('home')->with(['msg', 'Erro ao deletar banda.',
                                           'type_msg' => 'danger']);
    }
}
