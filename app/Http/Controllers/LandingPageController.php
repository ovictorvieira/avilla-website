<?php

namespace App\Http\Controllers;

use App\Services\BandsService;
use App\Services\UsersService;
use Illuminate\Http\Request;
use Alaouy\Youtube\Facades\Youtube;

class LandingPageController extends Controller
{
    /**
     * @var Youtube
     */
    protected $channel, $videoList;

    /**
     * @var BandsService
     */
    protected $bandsService;

    /**
     * @var UsersService
     */
    protected $membersService;

    public function __construct(BandsService $bandsService,
                                UsersService $membersService)
    {
        $this->bandsService = $bandsService;

        $this->membersService = $membersService;

        $youtubeIdChannel = 'UCW0JadVO0-b16noeEnJPu5A';
        // Get channel data by channel ID, return an STD PHP object
        $this->channel = Youtube::getChannelById($youtubeIdChannel);

        // List videos in a given channel, return an array of PHP objects
        $this->videoList = Youtube::listChannelVideos($youtubeIdChannel, 6);
    }

    public function index()
    {
        $band = $this->bandsService->getFirstBand();
        $members = $this->membersService->getAllUsers();

        return view('landingPage', ['videoList' => $this->videoList,
                                         'band' => $band,
                                         'members' => $members]);
    }

    /* Youtube Methods Publics*/

    /*

    $this->video = $this->youtubeApi->getVideoInfo('rie-hPVJ7Sw');

    // Get multiple videos info from an array
    $this->videoList = $this->youtubeApi->getVideoInfo(['rie-hPVJ7Sw','iKHTawgyKWQ']);

    // Get multiple videos related to a video
    $relatedVideos = $this->youtubeApi->getRelatedVideos('iKHTawgyKWQ');

    // Get comment threads by videoId
    $commentThreads = $this->youtubeApi->getCommentThreadsByVideoId('zwiUB_Lh3iA');

    // Get popular videos in a country, return an array of PHP objects
    $videoList = $this->youtubeApi->getPopularVideos('us');

    // Search playlists, channels and videos. return an array of PHP objects
    $results = $this->youtubeApi->search('Android');

    // Only search videos, return an array of PHP objects
    $videoList = $this->youtubeApi->searchVideos('Android');

    // Search only videos in a given channel, return an array of PHP objects
    $videoList = $this->youtubeApi->searchChannelVideos('keyword', 'UCk1SpWNzOs4MYmr0uICEntg', 40);

    $results = $this->youtubeApi->searchAdvanced([ // params  ]);

    // Get channel data by channel name, return an STD PHP object
    $channel = $this->youtubeApi->getChannelByName('xdadevelopers');

    // Get playlist by ID, return an STD PHP object
    $playlist = $this->youtubeApi->getPlaylistById('PL590L5WQmH8fJ54F369BLDSqIwcs-TCfs');

    // Get playlists by multiple ID's, return an array of STD PHP objects
    $playlists = $this->youtubeApi->getPlaylistById(['PL590L5WQmH8fJ54F369BLDSqIwcs-TCfs', 'PL590L5WQmH8cUsRyHkk1cPGxW0j5kmhm0']);

    // Get playlist by channel ID, return an array of PHP objects
    $playlists = $this->youtubeApi->getPlaylistsByChannelId('UCk1SpWNzOs4MYmr0uICEntg');

    // Get items in a playlist by playlist ID, return an array of PHP objects
    $playlistItems = $this->youtubeApi->getPlaylistItemsByPlaylistId('PL590L5WQmH8fJ54F369BLDSqIwcs-TCfs');

    // Get channel activities by channel ID, return an array of PHP objects
    $activities = $this->youtubeApi->getActivitiesByChannelId('UCk1SpWNzOs4MYmr0uICEntg');

    // Retrieve video ID from original YouTube URL
    $videoId = $this->youtubeApi->parseVidFromURL('https://www.youtube.com/watch?v=moSFlvxnbgk');
    // result: moSFlvxnbgk

    */
}
