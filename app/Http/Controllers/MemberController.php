<?php

namespace App\Http\Controllers;

use App\Models\TypesMusician;
use Exception;
use App\Services\BandsService;
use App\Services\TypesMusicianService;
use App\Services\UsersService;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * @var UsersService
     */
    protected $userService;

    /**
     * @var BandsService
     */
    protected $bandService;

    /**
     * @var TypesMusician
     */
    protected $typeMusicianService;

    /**
     * MemberController constructor.
     * @param UsersService $userService
     * @param BandsService $bandsService
     * @param TypesMusicianService $typesMusicianService
     */
    public function __construct(UsersService $userService,
                                BandsService $bandsService,
                                TypesMusicianService $typesMusicianService)
    {
        $this->userService = $userService;
        $this->bandService = $bandsService;
        $this->typeMusicianService = $typesMusicianService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->userService->getAllUsers();

        return view('musicians.index', ['users' => $users]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function new()
    {
        $members    = $this->userService->getAllUsers();
        $band       = $this->bandService->getFirstBand();
        $typesMusician = $this->typeMusicianService->getAllMusicians();

        return view('musicians.create', ['members' => $members,
                                              'band' => $band,
                                              'typesMusician' => $typesMusician]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($this->userService->createNewUser($request->all())) {

            return redirect('home')->with(['msg' => 'Integrante cadastrado com sucesso!',
                                              'type_msg' => 'success']);
        }

        return redirect('home')->with(['msg', 'Erro ao cadastrar integrante.',
                                          'type_msg' => 'danger']);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $member = $this->userService->getUserById($id);
        $members = $this->userService->getAllUsers();
        $band       = $this->bandService->getFirstBand();
        $typesMusician = $this->typeMusicianService->getAllMusicians();

        return view('musicians.show', ['member' => $member,
                                            'band' => $band,
                                            'members' => $members,
                                            'typesMusician' => $typesMusician]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        $members = $this->userService->getAllUsers();
        $band       = $this->bandService->getFirstBand();
        $typesMusician = $this->typeMusicianService->getAllMusicians();

        return view('musicians.edit', ['band' => $band,
                                            'members' => $members,
                                            'typesMusician' => $typesMusician]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if($this->userService->updateUser($id, $request->all())) {

            return redirect('home')->with(['msg' => 'Integrante cadastrado com sucesso!',
                                                        'type_msg' => 'success']);
        }

        return redirect('home')->with(['msg', 'Erro ao atualizar dados, tente novamente.',
                                                 'type_msg' => 'danger']);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        if($this->userService->deleteUser($id)) {

            return redirect('home')->with(['msg' => 'Integrante deletado com sucesso!',
                                              'type_msg' => 'success']);
        }

        return redirect('home')->with(['msg', 'Erro ao deletar integrante.',
                                          'type_msg' => 'danger']);
    }

}
