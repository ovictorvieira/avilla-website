<?php

namespace App\Repository;

use App\Models\Bands;
use Caffeinated\Repository\Repositories\EloquentRepository;

class BandsRepository extends EloquentRepository
{
    public $model = Bands::class;

    public $tag = ['band'];

    public function __construct()
    {
        parent::__construct();
    }

    public function getFirstBand()
    {
        return $this->findAll()[0];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getBandById($id)
    {
        return $this->find($id);
    }

    /**
     * @param $data
     * @return array
     */
    public function createNewBand($data)
    {
        return $this->create($data);
    }

    /**
     * @param $id
     * @param $data
     * @return array
     */
    public function updateBand($id, $data)
    {
        return $this->update($id, $data);
    }

    /**
     * @param $id
     * @return array
     */
    public function deleteBand($id)
    {
        return $this->delete($id);
    }
}
