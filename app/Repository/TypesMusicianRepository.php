<?php

namespace App\Repository;

use App\Models\TypesMusician;
use Caffeinated\Repository\Repositories\EloquentRepository;

class TypesMusicianRepository extends EloquentRepository
{
    /**
     * @var $model
     */
    public $model = TypesMusician::class;

    /**
     * @var array
     */
    public $tag = ['typeMusician'];

    /**
     * TypesMusicianRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getAllMusicians()
    {
        return $this->paginate(\Config::get('app.limit'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMusicianById($id)
    {
        return $this->find($id);
    }

    /**
     * @param $data
     * @return array
     */
    public function createNewMusician($data)
    {
        return $this->create($data);
    }

    /**
     * @param $id
     * @param $data
     * @return array
     */
    public function updateMusician($id, $data)
    {
        return $this->update($id, $data);
    }

    /**
     * @param $id
     * @return array
     */
    public function deleteMusician($id)
    {
        return $this->delete($id);
    }

}
