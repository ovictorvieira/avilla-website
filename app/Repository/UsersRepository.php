<?php

namespace App\Repository;

use App\Models\User;
use Caffeinated\Repository\Repositories\EloquentRepository;

class UsersRepository extends EloquentRepository
{
    protected $userModel;

    /**
     * UsersRepository constructor.
     * @param User $userModel
     */
    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

    /**
     * @return mixed
     */
    public function getAllUsers()
    {
        return $this->userModel->paginate(\Config::get('app.limit'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserById($id)
    {
        return $this->userModel->findOrFail($id);
    }

    /**
     * @param $data
     * @return array
     */
    public function newUser($data)
    {
        return $this->userModel->create($data);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateUser($id, $data)
    {
        $user = $this->userModel->findOrFail($id);

        return $user->fill($data)->save();
    }

    public function deleteUser($id)
    {
        return $this->userModel->destroy($id);
    }

}