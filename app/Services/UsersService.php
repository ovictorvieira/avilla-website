<?php

namespace App\Services;

use App\Repository\UsersRepository;
use App\Validator\UserValidator;
use Validator;

class UsersService
{
    /**
     * @var UsersRepository
     */
    protected $userRepository;

    /**
     * UsersService constructor.
     * @param UsersRepository $userRepository
     */
    public function __construct(UsersRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param $userFormContent
     * @return array
     */
    public function createNewUser($userFormContent)
    {
        return $this->userRepository->newUser($userFormContent);
    }

    /**
     * @return mixed
     */
    public function getAllUsers()
    {
        return $this->userRepository->getAllUsers();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserById($userId)
    {
        return $this->userRepository->getUserById($userId);
    }

    /**
     * @param $userId
     * @param $userFormContent
     * @return mixed
     */
    public function updateUser($userId, $userFormContent)
    {
        $validator = Validator::make($userFormContent, UserValidator::IMAGE_UPLOAD);

        if($validator->fails()) {
            return false;
        }

        if(empty($userFormContent['password'])) {

            $user = $this->userRepository->getUserById($userId);

            $password = $user->password;

            $userFormContent['password'] = $password;
        }

        if(!empty($userFormContent['principal_image'])) {

            $user = $this->getUserById($userId);

            $image = $userFormContent['principal_image'];

            $imageName = $image->hashName();

            if(!$image->storeAs('users/' . $user->id, $imageName)) {
                return false;
            }

            $userFormContent['principal_image'] = $imageName;
        }

        return $this->userRepository->updateUser($userId, $userFormContent);
    }

    /**
     * @param $userId
     * @return array
     */
    public function deleteUser($userId)
    {
        return $this->userRepository->deleteUser($userId);
    }

}