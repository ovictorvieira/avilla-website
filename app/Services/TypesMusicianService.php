<?php

namespace App\Services;

use App\Repository\TypesMusicianRepository;

class TypesMusicianService
{
    /**
     * @var TypesMusicianRepository
     */
    protected $typeMusicianRepository;

    /**
     * TypesMusicianService constructor.
     * @param TypesMusicianRepository $typesMusicianRepository
     */
    public function __construct(TypesMusicianRepository $typesMusicianRepository)
    {
        $this->typeMusicianRepository = $typesMusicianRepository;
    }

    /**
     * @return mixed
     */
    public function getAllMusicians()
    {
        return $this->typeMusicianRepository->getAllMusicians();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMusicianById($id)
    {
        return $this->typeMusicianRepository->getMusicianById($id);
    }

    /**
     * @param $data
     * @return array
     */
    public function createNewMusician($data)
    {
        return $this->typeMusicianRepository->createNewMusician($data);
    }

    /**
     * @param $id
     * @param $data
     * @return array
     */
    public function updateMusician($id, $data)
    {
        return $this->typeMusicianRepository->updateMusician($id, $data);
    }

    /**
     * @param $id
     * @return array
     */
    public function deleteMusician($id)
    {
        return $this->typeMusicianRepository->deleteMusician($id);
    }

}
