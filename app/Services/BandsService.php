<?php

namespace App\Services;

use App\Models\Bands;
use App\Repository\BandsRepository;

class BandsService
{
    /**
     * @var Bands
     */
    protected $bandsRepository;

    /**
     * BandsService constructor.
     * @param BandsRepository $bandsRepository
     */
    public function __construct(BandsRepository $bandsRepository)
    {
        $this->bandsRepository = $bandsRepository;
    }

    /**
     * @return mixed
     */
    public function getFirstBand()
    {
        return $this->bandsRepository->getFirstBand();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getBandById($id)
    {
        return $this->bandsRepository->getBandById($id);
    }

    /**
     * @param $data
     * @return array
     */
    public function createNewBandData($data)
    {
        return $this->bandsRepository->createNewBand($data);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateBand($id, $data)
    {
        return $this->bandsRepository->updateBand($id, $data);
    }

    /**
     * @param $id
     * @return array|bool|null
     * @throws \Exception
     */
    public function deleteBand($id)
    {
        return $this->bandsRepository->delete($id);
    }
}
