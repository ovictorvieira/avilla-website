<?php

namespace App\Models;

use App\Presenter\UsersPresenter;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laracasts\Presenter\PresentableTrait;

class User extends Authenticatable
{
    use Notifiable;

    use PresentableTrait;

    /**
     * @var string
     */
    protected $presenter = UsersPresenter::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'telephone',
        'password',
        'type_musician_id',
        'email',
        'social_media_facebook_link',
        'social_media_instagram_link',
        'social_media_twitter_link',
        'principal_image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function typeMusician()
    {
        return $this->belongsTo(TypesMusician::class);
    }
}
