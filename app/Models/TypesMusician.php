<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TypesMusician extends Model
{
    protected $table = 'types_musician';

    protected $fillable = ['musician'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
