<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bands extends Model
{
    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'description',
        'repertoire_summary',
        'responsible_name',
        'responsible_phone',
        'responsible_email',
        'social_media_facebook',
        'social_media_twitter',
        'social_media_instagram',
        'social_media_youtube',
    ];
}
