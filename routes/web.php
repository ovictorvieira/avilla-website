<?php

Route::get('/', 'LandingPageController@index')->name('landingPageShow');

Auth::routes();

Route::middleware('auth')->group(function() {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/band', 'BandController@index')->name('band');
    Route::get('/band/show', 'BandController@show')->name('bandShow');
    Route::get('/band/new', 'BandController@new')->name('bandCreate');
    Route::post('/band/store/', 'BandController@store')->name('bandStore');
    Route::get('/band/edit/{id}', 'BandController@edit')->name('bandEdit');
    Route::put('/band/update/{id}', 'BandController@update')->name('bandUpdate');
    Route::delete('/band/delete/{id}', 'BandController@delete')->name('bandDelete');

    Route::get('/members', 'MemberController@index')->name('members');
    Route::get('/member/show/{id}', 'MemberController@show')->name('memberShow');
    Route::get('/member/create/', 'MemberController@new')->name('memberNew');
    Route::post('/member/store/', 'MemberController@store')->name('memberStore');
    Route::get('/member/edit/{id}', 'MemberController@edit')->name('memberEdit');
    Route::put('/member/update/{id}', 'MemberController@update')->name('memberUpdate');
    Route::delete('/member/delete/{id}', 'MemberController@delete')->name('memberDelete');



});
